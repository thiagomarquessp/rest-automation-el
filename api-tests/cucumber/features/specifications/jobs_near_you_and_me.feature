# language: pt

Funcionalidade: Busca vagas próximas

Cenario: Realizar a busca de vagas próximas a mim e próximas de casa
	
	Dado que eu acesse o endpoint para token
	Quando eu fizer sign up
	E buscar uma vaga perto de mim
	Entao valido se a existe vagas
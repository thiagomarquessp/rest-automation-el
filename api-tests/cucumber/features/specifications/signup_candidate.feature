# language: pt


Funcionalidade: Cadastro de candidato

Cenario: Realizar o cadastro do candidato via api
	
	Dado que eu acesse o endpoint para token
	Quando eu fizer sign up
	Entao valido se o candidato foi criado corretamente
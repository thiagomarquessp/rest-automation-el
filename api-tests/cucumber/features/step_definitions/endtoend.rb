# encoding: utf-8
#!/usr/bin/env ruby
## Sign Up

Dado(/^que eu acesse o endpoint para token$/) do
	@phone = Faker::Base.numerify('1198#######').to_s
	puts @phone
	formBody = {:grant_type => 'client_credentials', :scope => 'candidate.signUp', :client_id => '5485dfee32e2d4643102a9b9_fvsa01ykit4w0cwc8s8kksgk0s4wc4kg88co4cwwkcs8gcss0', :client_secret => '51ewrfpgthk44kwg8owss8g80gkw04occ4go80o0csc0w8w88w'}
	@response = HTTParty.post("http://api.qa.empregoligado.net/token", :body=> formBody)
	#puts @response['access_token'].to_s
end

Quando(/^eu fizer sign up$/) do
  signupbody = {
    "name": Faker::Name.name,
    "phone": @phone,
    "gender": 0,
    "password": "inicial1234",
    "education": 4,
    "salary": 725,
    "negotiableSalary": true,
    "email": Faker::Internet.email,
    "cpf": "33440612830",
    "birthDate": "1986-06-03",
    "category1": 4,
    "position1": 5,
    "experience1": 4,
    "address": {
        "zip": "05433001"
    }
}.to_json
  @signup = HTTParty.post("http://api.qa.empregoligado.net/sign-up",
  	:body => signupbody,
  	:headers => {
    "Content-Type" => 'application/json',
    "Accept" => "application/json",
    "Authorization" => "Bearer " +@response['access_token'].to_s}
  	)
end

Entao(/^valido se o candidato foi criado corretamente$/) do
	candidate_body_token = {:scope => 'candidate candidate.basicInfo', :username =>  @phone, :password => 'inicial1234', :client_id => '5485dfee32e2d4643102a9b9_fvsa01ykit4w0cwc8s8kksgk0s4wc4kg88co4cwwkcs8gcss0', :client_secret => '51ewrfpgthk44kwg8owss8g80gkw04occ4go80o0csc0w8w88w', :grant_type => 'password'}
	@candidate_token = HTTParty.post("http://api.qa.empregoligado.net/token", :body=> candidate_body_token, :headers => {"Accept" => "application/json"})
	#puts @candidate_token['access_token']
	candidate_response =  HTTParty.get('https://api.qa.empregoligado.net/account', :headers => {
    "Content-Type" => 'application/json',
    "Accept" => "application/json",
    "Authorization" => "Bearer " + @candidate_token['access_token']})
	puts candidate_response
end

## Jobs Near You

Quando(/^buscar uma vaga perto de mim$/) do
  	#@response_a = HTTParty.get("http://api.qa.empregoligado.net/jobs?longitude=-46.6893&latitude=-23.5555", :headers => {"Accept" => 'application/json', "ContentType" => 'application/x-www-form-urlencoded', "Authorization" =>  'Bearer ' + @response['access_token'] })
	@response_a = HTTParty.get("http://api.qa.empregoligado.net/jobs?near-home=&limit=100&km-radius=100", :headers => {"Accept" => 'application/json', "ContentType" => 'application/x-www-form-urlencoded', "Authorization" =>  'Bearer ' + @response['access_token']})
end

Entao(/^valido se a existe vagas$/) do
  pending # Write code here that turns the phrase above into concrete actions
end
